# Usage
Requires two terminals open

In one terminal run the server:
`npx tsc; node .\dist\server.js`

In the other run the experiment:
`npx rollup -c .\rollup.config.js; npx tsc; node .\dist\main.js https://www.trademe.co.nz/a`