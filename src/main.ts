import { chromium } from 'playwright';

const page_url = process.argv[2];
console.log(page_url)

async function run() {
    const browser = await chromium.launch();
    const page = await browser.newPage();
    await browser.startTracing(page, {
        path: 'out/',
        screenshots: true,
    });

    await page.addInitScript({
        path: './dist/web-vitals-collecter.js'
    });
    //   await page.goto('http://whatsmyuseragent.org/');
    await page.goto(page_url);
    await page.click('body');
    await page.waitForLoadState('networkidle');
    await page.waitForTimeout(5000);
    await page.screenshot({ path: `example.png` });
    await page.reload();
    await browser.stopTracing();
    await page.waitForLoadState('networkidle');
    await page.close();
    await browser.close();
}

async function main() {
    const trials = 5;
    for (let i = 0; i < trials; i++) {
        await run();
    }
}

main();