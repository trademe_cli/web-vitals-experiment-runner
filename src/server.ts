import http from 'http';
import Koa from 'koa';
import Router from '@koa/router';
import bodyParser from 'koa-bodyparser';
import cors from '@koa/cors';


enum Metric {
  CLS = "CLS",
  FID = "FID",
  LCP = "LCP",
}
type MetricHistory = { [key in Metric]: number[] }

const metricsHistory: MetricHistory = {
    CLS: [],
    FID: [],
    LCP: []
}


type MetricsInstance= { [key in Metric]: number }

async function main(): Promise<void> {
    const app = new Koa();
    const router = new Router();

    app.use(cors())
    app.use(bodyParser());

    router.post('/metric', async (ctx, next) => {
        const body = ctx.request.body as MetricsInstance
        console.log(body);
        ctx.body = body

        for (const key of Object.values(Metric)) {
            metricsHistory[key].push(body[key]);
        }

        await next();
    });

    router.get('/metric', async (ctx, next) => {
        console.log(metricsHistory);
        const entries = Object.values(Metric)
            .map(metric => {
                const metric_history = metricsHistory[metric];
                const metric_sum = metric_history.reduce((a, x) => a + x);
                const metric_average = metric_sum / metric_history.length;
                return [
                    metric,
                    metric_average
                ]
            });

        const averageMetrics = Object.fromEntries(entries);
        console.log(averageMetrics)

        ctx.body = averageMetrics
        await next();
    });



    app.use(router.routes());
    app.use(router.allowedMethods());

    app.listen(3000);
}


main()