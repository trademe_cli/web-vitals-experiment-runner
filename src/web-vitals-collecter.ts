import {getCLS, getFID, getLCP} from 'web-vitals';

const metrics = {
    CLS: null,
    FID: null,
    LCP: null
}


async function sendMetrics() {
    console.log(JSON.stringify(metrics));

    // If any metrics haven't be set yet then hol' up.
    if (!metrics.CLS || !metrics.FID || !metrics.LCP) {
        return;
    }

    const url = `http://localhost:3000/metric`;
    const response = await fetch(url, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(metrics),
        keepalive: true
    })

    console.log(response.json());
}

async function updateMetric(metric) {
    console.log(metric);
    metrics[metric.name] = metric.value;
    await sendMetrics();
}

getCLS(updateMetric);
getFID(updateMetric);
getLCP(updateMetric);