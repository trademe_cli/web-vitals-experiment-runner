"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const koa_1 = __importDefault(require("koa"));
const router_1 = __importDefault(require("@koa/router"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const cors_1 = __importDefault(require("@koa/cors"));
var Metric;
(function (Metric) {
    Metric["CLS"] = "CLS";
    Metric["FID"] = "FID";
    Metric["LCP"] = "LCP";
})(Metric || (Metric = {}));
const metricsHistory = {
    CLS: [],
    FID: [],
    LCP: []
};
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = new koa_1.default();
        const router = new router_1.default();
        app.use(cors_1.default());
        app.use(koa_bodyparser_1.default());
        router.post('/metric', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
            const body = ctx.request.body;
            console.log(body);
            ctx.body = body;
            for (const key of Object.values(Metric)) {
                metricsHistory[key].push(body[key]);
            }
            yield next();
        }));
        router.get('/metric', (ctx, next) => __awaiter(this, void 0, void 0, function* () {
            console.log(metricsHistory);
            const entries = Object.values(Metric)
                .map(metric => {
                const metric_history = metricsHistory[metric];
                const metric_sum = metric_history.reduce((a, x) => a + x);
                const metric_average = metric_sum / metric_history.length;
                return [
                    metric,
                    metric_average
                ];
            });
            const averageMetrics = Object.fromEntries(entries);
            console.log(averageMetrics);
            ctx.body = averageMetrics;
            yield next();
        }));
        app.use(router.routes());
        app.use(router.allowedMethods());
        app.listen(3000);
    });
}
main();
