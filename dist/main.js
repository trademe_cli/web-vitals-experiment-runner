"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const playwright_1 = require("playwright");
const page_url = process.argv[2];
console.log(page_url);
function run() {
    return __awaiter(this, void 0, void 0, function* () {
        const browser = yield playwright_1.chromium.launch();
        const page = yield browser.newPage();
        yield browser.startTracing(page, {
            path: 'out/',
            screenshots: true,
        });
        yield page.addInitScript({
            path: './dist/web-vitals-collecter.js'
        });
        //   await page.goto('http://whatsmyuseragent.org/');
        yield page.goto(page_url);
        yield page.click('body');
        yield page.waitForLoadState('networkidle');
        yield page.waitForTimeout(5000);
        yield page.screenshot({ path: `example.png` });
        yield page.reload();
        yield browser.stopTracing();
        yield page.waitForLoadState('networkidle');
        yield page.close();
        yield browser.close();
    });
}
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const trials = 5;
        for (let i = 0; i < trials; i++) {
            yield run();
        }
    });
}
main();
