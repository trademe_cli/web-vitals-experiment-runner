(function () {
    'use strict';

    var t,e,n,i,a=function(t,e){return {name:t,value:void 0===e?-1:e,delta:0,entries:[],id:"v1-".concat(Date.now(),"-").concat(Math.floor(8999999999999*Math.random())+1e12)}},r=function(t,e){try{if(PerformanceObserver.supportedEntryTypes.includes(t)){var n=new PerformanceObserver((function(t){return t.getEntries().map(e)}));return n.observe({type:t,buffered:!0}),n}}catch(t){}},o=!1,c=function(t,e){o||"undefined"!=typeof InstallTrigger||(addEventListener("beforeunload",(function(){})),o=!0);addEventListener("visibilitychange",(function n(i){"hidden"===document.visibilityState&&(t(i),e&&removeEventListener("visibilitychange",n,!0));}),!0);},u=function(t){addEventListener("pageshow",(function(e){e.persisted&&t(e);}),!0);},f="function"==typeof WeakSet?new WeakSet:new Set,s=function(t,e,n){var i;return function(){e.value>=0&&(n||f.has(e)||"hidden"===document.visibilityState)&&(e.delta=e.value-(i||0),(e.delta||void 0===i)&&(i=e.value,t(e)));}},m=function(t,e){var n,i=a("CLS",0),o=function(t){t.hadRecentInput||(i.value+=t.value,i.entries.push(t),n());},f=r("layout-shift",o);f&&(n=s(t,i,e),c((function(){f.takeRecords().map(o),n();})),u((function(){i=a("CLS",0),n=s(t,i,e);})));},d=-1,v=function(){return "hidden"===document.visibilityState?0:1/0},p=function(){c((function(t){var e=t.timeStamp;d=e;}),!0);},l=function(){return d<0&&(d=v(),p(),u((function(){setTimeout((function(){d=v(),p();}),0);}))),{get timeStamp(){return d}}},h={passive:!0,capture:!0},y=new Date,g=function(i,a){t||(t=a,e=i,n=new Date,L(removeEventListener),E());},E=function(){if(e>=0&&e<n-y){var a={entryType:"first-input",name:t.type,target:t.target,cancelable:t.cancelable,startTime:t.timeStamp,processingStart:t.timeStamp+e};i.forEach((function(t){t(a);})),i=[];}},w=function(t){if(t.cancelable){var e=(t.timeStamp>1e12?new Date:performance.now())-t.timeStamp;"pointerdown"==t.type?function(t,e){var n=function(){g(t,e),a();},i=function(){a();},a=function(){removeEventListener("pointerup",n,h),removeEventListener("pointercancel",i,h);};addEventListener("pointerup",n,h),addEventListener("pointercancel",i,h);}(e,t):g(e,t);}},L=function(t){["mousedown","keydown","touchstart","pointerdown"].forEach((function(e){return t(e,w,h)}));},T=function(n,o){var m,d=l(),v=a("FID"),p=function(t){t.startTime<d.timeStamp&&(v.value=t.processingStart-t.startTime,v.entries.push(t),f.add(v),m());},S=r("first-input",p);m=s(n,v,o),S&&c((function(){S.takeRecords().map(p),S.disconnect();}),!0),S&&u((function(){var r;v=a("FID"),m=s(n,v,o),i=[],e=-1,t=null,L(addEventListener),r=p,i.push(r),E();}));},b=function(t,e){var n,i=l(),o=a("LCP"),m=function(t){var e=t.startTime;e<i.timeStamp&&(o.value=e,o.entries.push(t)),n();},d=r("largest-contentful-paint",m);if(d){n=s(t,o,e);var v=function(){f.has(o)||(d.takeRecords().map(m),d.disconnect(),f.add(o),n());};["keydown","click"].forEach((function(t){addEventListener(t,v,{once:!0,capture:!0});})),c(v,!0),u((function(i){o=a("LCP"),n=s(t,o,e),requestAnimationFrame((function(){requestAnimationFrame((function(){o.value=performance.now()-i.timeStamp,f.add(o),n();}));}));}));}};

    const metrics = {
        CLS: null,
        FID: null,
        LCP: null
    };


    async function sendMetrics() {
        console.log(JSON.stringify(metrics));

        // If any metrics haven't be set yet then hol' up.
        if (!metrics.CLS || !metrics.FID || !metrics.LCP) {
            return;
        }

        const url = `http://localhost:3000/metric`;
        const response = await fetch(url, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(metrics),
            keepalive: true
        });

        console.log(response.json());
    }

    async function updateMetric(metric) {
        console.log(metric);
        metrics[metric.name] = metric.value;
        await sendMetrics();
    }

    m(updateMetric);
    T(updateMetric);
    b(updateMetric);

}());
