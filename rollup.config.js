// rollup.config.js
import typescript from '@rollup/plugin-typescript';
import { nodeResolve } from '@rollup/plugin-node-resolve';

export default {
  input: 'src/web-vitals-collecter.ts',
  output: {
    dir: 'dist',
    format: 'iife'
  },
  plugins: [typescript(), nodeResolve()]
};